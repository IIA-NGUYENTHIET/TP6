package problemes.cruche;

import iia.espacesEtats.modeles.Etat;
import java.util.Collection;
import java.util.LinkedList;

public class EtatCruche implements Etat {

    // Constantes ----------------------------------------------------------

    // Attributs -----------------------------------------------------------
    /**
     * Chaque cruche est représentée par un entier, qui est sa valeur max. Il y a
     * aussi sa quantité actuelle.
     */
    int capP;
    int capG;
    int nivP;
    int nivG;

    // Constructeurs -----------------------------------------------------------
    /**
     * Par d�faut toutes les tortues sont sur le dos sauf la troisi�me et la
     * quatri�me en partant de la gauche.
     */
    public EtatCruche() {
        this.capP = 5;
        this.capG = 7;
        this.nivP = 0;
        this.nivG = 0;
    }

    /**
     * Constructeur dupliquant � l'identique un EtatTortue
     * 
     * @param et : un EtatTortue
     */
    public EtatCruche(EtatCruche ec) {
        this.capP = ec.getCapP();
        this.capG = ec.getCapG();
        this.nivP = ec.getNivP();
        this.nivG = ec.getNivG();
    }

    // Accesseurs -----------------------------------------------------------
    public int getCapP() {
        return this.capP;
    }

    public int getCapG() {
        return this.capG;
    }

    public int getNivP() {
        return this.nivP;
    }

    public int getNivG() {
        return this.nivG;
    }

    // Modifieurs -----------------------------------------------------------
    public void remplirP() throws Exception {
        if (this.capP == this.nivP) {
            throw new Exception();
        } else {
            this.nivP = this.capP;
        }
    }

    public void remplirG() throws Exception {
        if (this.capG == this.nivG) {
            throw new Exception();
        } else {
            this.nivG = this.capG;
        }
    }

    public void viderP() throws Exception {
        if (0 == this.nivP) {
            throw new Exception();
        } else {
            this.nivP = 0;
        }
    }

    public void viderG() throws Exception {
        if (0 == this.nivG) {
            throw new Exception();
        } else {
            this.nivG = 0;
        }
    }

    public void verserPaG() throws Exception {
        if (this.nivP <= 0 || this.nivG >= this.capG) {
            throw new Exception();
        } else {
            int old_nivG = this.nivG;
            this.nivG = Math.min(this.capG, this.nivG + this.nivP);
            this.nivP = Math.max(0, this.nivP - (this.capG - old_nivG));
        }
    }

    public void verserGaP() throws Exception {
        if (this.nivG <= 0 || this.nivP >= this.capP) {
            throw new Exception();
        } else {
            int old_nivP = this.nivP;
            this.nivP = Math.min(this.capP, this.nivP + this.nivG);
            this.nivG = Math.max(0, this.nivG - (this.capP - old_nivP));
        }
    }

    /* ********** Methodes requises par l'interface Etat ************* */
    @Override
    public Collection<Etat> successeurs() {
        LinkedList<Etat> successeurs = new LinkedList<Etat>();

        try {
            EtatCruche succ = new EtatCruche(this);
            succ.remplirP();
            successeurs.add(succ);
        } catch (Exception e) {
            ;
        }

        try {
            EtatCruche succ = new EtatCruche(this);
            succ.remplirG();
            successeurs.add(succ);
        } catch (Exception e) {
            ;
        }

        try {
            EtatCruche succ = new EtatCruche(this);
            succ.viderP();
            successeurs.add(succ);
        } catch (Exception e) {
            ;
        }

        try {
            EtatCruche succ = new EtatCruche(this);
            succ.viderG();
            successeurs.add(succ);
        } catch (Exception e) {
            ;
        }

        try {
            EtatCruche succ = new EtatCruche(this);
            succ.verserPaG();
            successeurs.add(succ);
        } catch (Exception e) {
            ;
        }

        try {
            EtatCruche succ = new EtatCruche(this);
            succ.verserGaP();
            successeurs.add(succ);
        } catch (Exception e) {
            ;
        }

        return successeurs;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EtatCruche) {
            EtatCruche ec = (EtatCruche) obj;
            return this.nivP == ec.getNivP() && this.nivG == ec.getNivG() && this.capP == ec.getCapP()
                    && this.capG == ec.getCapG();
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "P : " + this.nivP + "\t G : " + this.nivG;
    }
}
