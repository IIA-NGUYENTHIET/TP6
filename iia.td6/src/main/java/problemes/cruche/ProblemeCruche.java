package problemes.cruche;

import iia.espacesEtats.modeles.Etat;
import iia.espacesEtats.modeles.Probleme;

/**
 * Mod�lisation du probl�me des tortues
 * 
 * @author Philippe Chatalic
 */
public class ProblemeCruche implements Probleme {

    // Attributs -------------------------------------------------------
    private EtatCruche etatInitial;
    private int but;

    private String descriptionBut = "But : avoir une cruche avec " + but + " litres d'eau";

    // Constructeurs -------------------------------------------------------
    public ProblemeCruche(Etat eInit,int but) {
        try {
            this.but = but;
            this.etatInitial = (EtatCruche) eInit;
        } catch (Exception e) {
            throw new RuntimeException("Un ProblemeCruche doit se créer à partir d'un EtatCruche ! ");
        }
    }

    // M�thodes requises par l'interface Probleme

    @Override
    public Etat getEtatInitial() {
        return etatInitial;
    }

    @Override
    public boolean isTerminal(Etat e) {
        if (!(e instanceof EtatCruche)) {
            return false;
        }
        EtatCruche ec = (EtatCruche) e;
        return this.but == ec.getNivG() || this.but == ec.getNivP();

    }

    /** M�thodes abstraites de la classe Probl�me � instancier **/
    @Override
    public String toString() {
        return "Etat initial : " + etatInitial + "\n" + descriptionBut;
    }

}
