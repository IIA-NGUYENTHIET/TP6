package iia.espacesEtats.algorithmes;

import java.util.*;

import iia.espacesEtats.modeles.Probleme;
import iia.espacesEtats.modeles.Etat;
import iia.espacesEtats.modeles.Solution;

/**
 * RechercheEnProfondeurDetectionDeCycles une version de l'algorithme de
 * recherche en profondeur d'abord capable de d�tecter les cycles pour ne pas
 * red�velopper des �tats d�j� vus pr�c�demments sur le chemin en cours
 * d'exploration
 *
 * @author <Vous M�me>
 */
public class RechercheEnProfondeurDetectionDeCycles extends AlgorithmeRechercheEE {

    // ----------------------------------------------------
    // Constructeurs
    // ----------------------------------------------------
    public RechercheEnProfondeurDetectionDeCycles() {
        super();
    }

    // ----------------------------------------------------
    // Methode(s) requise par la classe abstraite AlgorithmeRechercheEE
    // ----------------------------------------------------
    @Override
    public Solution chercheSolution(Probleme p) {
        List<Etat> dejaVu = new LinkedList<Etat>();

        // <A vous de compl�ter >
        return rProfSansCycle(p.getEtatInitial(), dejaVu, p);
    }

    private Solution rProfSansCycle(Etat s, List<Etat> dejaVu, Probleme p) {
        if (p.isTerminal(s)) {
            return new Solution(s);
        } else {
            for (Etat succ : s.successeurs())
                if (!(dejaVu.contains(succ))) {
                    dejaVu.add(s);
                    nbNoeudsDeveloppes++;
                    Solution res = rProfSansCycle(succ, dejaVu, p);

                    if (res != null) {
                        res.add(s);
                        return res;
                    }
                }
            return null;
        }
    }

}
