package iia.espacesEtats.algorithmes;

import java.util.*;

import iia.espacesEtats.modeles.Probleme;
import iia.espacesEtats.modeles.Solution;
import iia.espacesEtats.modeles.Etat;

/**
 * RechercheEnProfondeurSimple une impl�mentation simple de l'algorithme de
 * recherche en profondeur d'abord, sans d�tection de cycles, donc uniquement
 * adapt�e aux espaces d'�tats sans cycles ni branches infinies.
 * 
 * @author <Vous m�me>
 */
public class RechercheEnProfondeurSimple extends AlgorithmeRechercheEE {

    // ----------------------------------------------------
    // Constructeurs
    // ----------------------------------------------------

    public RechercheEnProfondeurSimple() {
        super();
    }

    // ----------------------------------------------------
    // Methode(s) requise par la classe abstraite AlgorithmeRechercheEE
    // ----------------------------------------------------

    @Override
    public Solution chercheSolution(Probleme p) {
        return rProf(p.getEtatInitial(), p);
    }

    private Solution rProf(Etat s, Probleme p) {
        if (p.isTerminal(s)) {
            return new Solution(s);
        } else {
            for (Etat succ : s.successeurs()) {
                nbNoeudsDeveloppes++;
                Solution res = rProf(succ, p);

                if (res != null) {
                    res.add(s);
                    return res;
                }
            }
            return null;
        }
    }

}
