package iia.espacesEtats.algorithmes;

import java.util.*;

import iia.espacesEtats.modeles.Probleme;
import iia.espacesEtats.modeles.Etat;
import iia.espacesEtats.modeles.Solution;
import iia.espacesEtats.graphes.Noeud;

/**
 * RechercheEnLargeur codage de l'algorithme de recherche en largeur d'abord
 * 
 * @author NGUYEN THIET Lam
 */
public class RechercheEnLargeur extends AlgorithmeRechercheEE {
    // Initialisation des structures de données
    Set<Noeud> dejaVu = new HashSet<Noeud>();
    Queue<Noeud> frontiere = new LinkedList<Noeud>();

    // ----------------------------------------------------
    // Constructeurs
    // ----------------------------------------------------
    public RechercheEnLargeur() {
        super();
    }

    // ----------------------------------------------------
    // Methode(s) requise par la classe abstraite AlgorithmeRechercheEE
    // ----------------------------------------------------
    @Override
    public Solution chercheSolution(Probleme p) {
        Noeud noeud_initial = new Noeud(p.getEtatInitial(), null);

        // Ajout de l'état initial à la frontiere.
        if (!(frontiere.add(noeud_initial))) {
            // Pour une raison on n'a pas pu ajouter l'état initial, on renvoie null
            return null;
        } else {
            while (!(frontiere.isEmpty())) {
                // On prend le premier noeud à gérer
                Noeud n = frontiere.poll();
                // L'état correspondant à ce noeud
                Etat n_etat = n.getEtat();

                if (p.isTerminal(n_etat)) {
                    return construireSolution(n);
                } else {
                    // Le noeud n sera traité
                    dejaVu.add(n);

                    // On calcule ses successeurs
                    Collection<Etat> successeurs = n_etat.successeurs();
                    for (Etat etat_succeseur : successeurs) {
                        // On regarde si le noeud n'est pas déja vu
                        if (!(estDansDejaVuOuFrontiere(etat_succeseur))) {
                            // On en fait un nouveau
                            Noeud nouveau_noeud = new Noeud(etat_succeseur,n);
                            this.frontiere.add(nouveau_noeud);
                            nbNoeudsDeveloppes++;
                        }
                    }
                }
            }
            // Pas de chamin trouvé
            return null;
        }

    }

    private boolean estDansDejaVuOuFrontiere(Etat e1) {
        for (Noeud n : dejaVu) {
            Etat e2 = n.getEtat();
            if (e1.equals(e2)) {
                return true;
            }
        }
        for (Noeud n : frontiere) {
            Etat e2 = n.getEtat();
            if (e1.equals(e2)) {
                return true;
            }
        }
        return false;

    }

    /**
     * Construit une solution à partir du noeud n en remontant les péres
     */
    public Solution construireSolution(Noeud n) {
        Solution solution = new Solution(n.getEtat());
        Noeud p = n;
        while (p.getPere() != null) {
            p = p.getPere();
            solution.add(p.getEtat());
        }
        return solution;
    }

}
